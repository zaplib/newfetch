﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFetchTests.Model
{
    class ModelHttpBin
    {
        public string origin { get; set; }
        public string url { get; set; }
        public string data { get; set; }

        public ModelHttpBinQuery args { get; set; }
        public ModelHttpBinQuery json { get; set; }
        public ModelHttpBinHeader headers { get; set; }
        public ModelHttpBinQuery form { get; set; }
    }

    class ModelHttpBinHeader
    {
        public string Accept { get; set; }
        public string Host { get; set; }
        public string Test { get; set; }
        [JsonProperty(PropertyName = "User-Agent")]
        public string UserAgent { get; set; }
        [JsonProperty(PropertyName = "Content-Type")]
        public string ContentType { get; set; }
        [JsonProperty(PropertyName = "Content-Length")]
        public long ContentLength { get; set; }
    }

    class ModelHttpBinQuery
    {
        public string name { get; set; }
        public int id { get; set; }
    }
}
