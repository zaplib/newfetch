﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewFetch;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFetch.Tests
{
    [TestClass()]
    public class CastTests
    {
        [TestMethod()]
        public void IsGenericTypeTest()
        {
            Trace.WriteLine("int = 16");
            Assert.IsTrue(Cast.IsType<int>(16));
            Trace.WriteLine("string = null");
            Assert.IsTrue(Cast.IsType<string>(null));
            Trace.WriteLine("int != null");
            Assert.IsFalse(Cast.IsType<int>(null));
            Trace.WriteLine("IDictionary = new Dictionary<string,string>()");
            Assert.IsTrue(Cast.IsType<IDictionary>(new Dictionary<string, string>()));
            Trace.WriteLine("object = \"Test\"");
            Assert.IsTrue(Cast.IsType<object>("Test"));
            Trace.WriteLine("List<int> =! new int[5]");
            Assert.IsFalse(Cast.IsType<List<int>>(new int[5]));
        }

        [TestMethod()]
        public void IsSomeObjectTypeTest()
        {
            Trace.WriteLine("20 = 16");
            Assert.IsTrue(Cast.IsType(16, 20));
            Trace.WriteLine("\"Test\" = null");
            Assert.IsTrue(Cast.IsType(null, "Test"));
            Trace.WriteLine("null != 10");
            Assert.IsFalse(Cast.IsType(10, null));
            Trace.WriteLine("new Dictionary<int,int>() != new Dictionary<string,string>()");
            Assert.IsFalse(Cast.IsType(new Dictionary<string, string>(), new Dictionary<int, int>()));
            Trace.WriteLine("new object() = \"Test\"");
            Assert.IsTrue(Cast.IsType("Test", new object()));
            Trace.WriteLine("new List<int>() =! new int[5]");
            Assert.IsFalse(Cast.IsType(new int[5], new List<int>()));
            Trace.WriteLine("null = null");
            Assert.IsTrue(Cast.IsType(null, null));
        }

        [TestMethod()]
        public void ToTest()
        {
            int res1 = Cast.To<int>("123");
            Trace.WriteLine(res1);
            string res2 = Cast.To<string>(123);
            Trace.WriteLine(res2);
            bool res3 = Cast.To<bool>(1);
            Trace.WriteLine(res3);
            int res4 = Cast.To<int>("Test");
            Trace.WriteLine(res4);
            int res5 = Cast.To<int>("Test", 99999);
            Trace.WriteLine(res5);

            Assert.AreEqual(123, res1);
            Assert.AreEqual("123", res2);
            Assert.AreEqual(true, res3);
            Assert.AreEqual(0, res4);
            Assert.AreEqual(99999, res5);
        }

        [TestMethod()]
        public void ToSpecialTypeTest()
        {
            // 指定轉型
            object test1 = "123";
            int res1 = (int)Cast.To(test1, typeof(int));
            Trace.WriteLine(res1);
            // 安全強轉
            object test2 = "true";
            bool res2 = (bool)Cast.To(test2, typeof(bool));
            Trace.WriteLine(res2);
            // 安全強轉失敗 + 型態預設輸出
            object test3 = new List<int>() { 1, 2, 3, 4, 5 };
            Dictionary<string,string> res3 = (Dictionary<string, string>)Cast.To(test3, typeof(Dictionary<string, string>));
            Trace.WriteLine(JsonConvert.SerializeObject(res3));
            // 安全強轉失敗 + 型態預設輸出
            object test4 = new List<int>() { 1, 2, 3, 4, 5 };
            int res4 = (int)Cast.To(test4,typeof(int));
            Trace.WriteLine(res4);

            Assert.AreEqual(123, res1);
            Assert.AreEqual(true, res2);
            Assert.AreEqual(null, res3);
            Assert.AreEqual(0, res4);
        }
    }
}