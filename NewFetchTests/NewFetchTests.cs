﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewFetch;
using NewFetchTests.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFetch.Tests
{
    [TestClass()]
    public class NewFetchTests
    {
        [TestMethod()]
        public void GetTextTest()
        {
            NewFetch f = new NewFetch("http://httpbin.org/get");

            f.Header = new Dictionary<string, string>() {
                {"User-Agent","my browser" },
                { "Test", "123456"}
            };

            f.Header = new Dictionary<string, string>() {
                {"User-Agent","my yoyo browser" },
                { "Test", "987654321"}
            };

            f.Accept = "text/html";
            f.Accept = "wefewfwefwefw";
            f.Accept = "application/json";

            string res = f.Get(new { name = "zap", test = (string)null });

            Trace.WriteLine(res);
            Assert.IsNotNull(res);
        }

        [TestMethod()]
        public void GetModelTest()
        {
            NewFetch f = new NewFetch("http://httpbin.org/get");
            f.Header = new Dictionary<string, string>() {
                {"User-Agent","my browser" },
                { "Test", "123456"}
            };

            f.Accept = "text/html";
            f.Accept = "wefewfwefwefw";
            f.Accept = "application/json";

            ModelHttpBin res = f.Get<ModelHttpBin>(new { name = "zap" });

            Trace.WriteLine(JsonConvert.SerializeObject(res));
            Assert.IsNotNull(res);
        }

        [TestMethod()]
        public void GetFailTest()
        {
            NewFetch f = new NewFetch("http://httpbin.org/post");
            string res = f.Get();
            if (res == null) res = f.GetResponse();
            Trace.WriteLine(res);
            Assert.IsNotNull(res);
        }

        [TestMethod()]
        public void GetResponseHeaderTest()
        {
            NewFetch f = new NewFetch("http://httpbin.org/get");
            f.Get();
            string ResponseContentType = f.GetResponseHeader("Access-Control-Allow-Origin");
            Trace.WriteLine(JsonConvert.SerializeObject(f.GetResponseHeaders()));
            Assert.AreEqual("*", ResponseContentType);
        }

        [TestMethod()]
        public void GetBinaryTest()
        {
            NewFetch f = new NewFetch();
            f.Url = "http://www.iqs-t.com/Pub/images/site/logo.png";
            f.MaxDownloadSize = 5 * 1024;
            byte[] img = f.GetBinary();
            Assert.IsNotNull(img);
            File.WriteAllBytes(@"./" + Path.GetFileName(f.Url), img);
        }

        [TestMethod()]
        public void PostJsonDataTextResponseTest()
        {
            NewFetch f = new NewFetch("http://httpbin.org/post");

            f.Header = new Dictionary<string, string>() {
                {"User-Agent","my browser" },
                { "Test", "123456"}
            };

            string res = f.Post(
                new { test = "zap", id = 99, is_admin = true, now = DateTime.Now, permission = new int[] { 1, 2, 3 } },
                new { limit = 100 });

            Assert.IsNotNull(res);
            Trace.WriteLine(res);
        }

        [TestMethod()]
        public void PostJsonDataModelResponseTest()
        {
            NewFetch f = new NewFetch("http://httpbin.org/post");
            f.Header = new Dictionary<string, string>() {
                {"User-Agent","my browser" },
                { "Test", "123456"}
            };

            ModelHttpBin res = f.Post<ModelHttpBin>(
              new { name = "zap", id = 99, is_admin = true, now = DateTime.Now, permission = new int[] { 1, 2, 3 } },
              new { id = 100, name = "argname" });

            Assert.IsNotNull(res);
            Trace.WriteLine(JsonConvert.SerializeObject(res));
        }

        [TestMethod()]
        public void DeleteURLEncodeDataTextResponseTest()
        {
            NewFetch f = new NewFetch("http://httpbin.org/delete");
            f.Header = new Dictionary<string, string>() {
                {"User-Agent","my browser" },
                { "Test", "123456"}
            };

            f.ContentType = "application/x-www-form-urlencoded";
            string res = f.Delete(
                new { name = "zap", id = 99 },
                new { id = 100, name = "argname" }
            );

            Assert.IsNotNull(res);
            Trace.WriteLine(res);
        }

        [TestMethod()]
        public void DeleteMultipartFormDataTextResponseTest()
        {
            NewFetch f = new NewFetch("http://httpbin.org/delete");
            f.Header = new Dictionary<string, string>() {
                {"User-Agent","my browser" },
                { "Test", "123456"}
            };
            string res = f.Delete(
                new { name = "zap", id = 99 },
                new { id = 100, name = "argname" },
                new { file1 = Directory.GetCurrentDirectory() + @"/file.txt" }
            );
            Assert.IsNotNull(res);
            Trace.WriteLine(res);
        }

        [TestMethod()]
        public void PutMultipartFormDataTextResponseTest()
        {
            NewFetch f = new NewFetch("http://httpbin.org/put");
            f.Header = new Dictionary<string, string>() {
                {"User-Agent","my browser" },
                { "Test", "123456"}
            };
            f.ContentType = "multipart/form-data";
            string res = f.Put(
                new { name = "zap", id = 99 },
                new { id = 100, name = "argname" }
            );
            Assert.IsNotNull(res);
            Trace.WriteLine(res);
        }

        [TestMethod()]
        public void CustomSendTest()
        {
            NewFetch f = new NewFetch("http://httpbin.org/patch");
            f.Request.Method = new System.Net.Http.HttpMethod("Patch");
            f.ContentType = "multipart/form-data";
            f.Request.Content = f.BuildMultipartFormDataContent(new { name = "zap", id = 99 });
            if (f.Send())
            {
                Trace.WriteLine("ResponseHeader: " + JsonConvert.SerializeObject(f.GetResponseHeaders()));
                Trace.WriteLine("Response: " + f.GetResponse());
            }
            else
            {
                Trace.WriteLine("StatusCode: " + f.StatusCode);
                Trace.WriteLine(f.GetResponse());
            }

        }

        [TestMethod()]
        public void CookieTest()
        {
            NewFetch f = new NewFetch("https://term.ptt.cc/");
            f.Cookie = new { Test = "Test-Cookie", NewFetch = "NewFetch" };
            string res = f.Get();
            Assert.IsNotNull(res);
            Trace.WriteLine("Cookie: " + JsonConvert.SerializeObject(f.Cookie));
        }

    }



}