﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewFetch;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFetch.Tests
{
    [TestClass()]
    public class MirrorTests
    {

        [TestMethod()]
        public void ModelMembersTest()
        {
            ModelTest model = new ModelTest()
            {
                name = "yoyoy",
                id = 10,
                permission = new List<int>() { 1, 2, 3, 4 }
            };
            foreach (var d in Mirror.Members(model))
            {
                if (d.Key.ToString() == "name") Assert.IsTrue((string)d.Value == "yoyoy");
                if (d.Key.ToString() == "id") Assert.IsTrue((int)d.Value == 10);
                if (d.Key.ToString() == "permission") Assert.IsNotNull(d.Value);
            }
        }
        [TestMethod()]
        public void DectionaryMembersTest()
        {
            Dictionary<string, string> data = new Dictionary<string, string>() {
                { "name","yoyo"},
                 { "id","10"}
            };
            foreach (var d in Mirror.Members(data))
            {
                if (d.Key.ToString() == "name") Assert.IsTrue((string)d.Value == "yoyo");
                if (d.Key.ToString() == "id") Assert.IsTrue((string)d.Value == "10");
            }
        }

        [TestMethod()]
        public void AssignObjectTest()
        {
            ModelTest data = new ModelTest()
            {
                id = 0,
                name = null,
                permission = null
            };

            var data2 = new
            {
                id = 99,
                name = "Zap",
            };

            var data3 = new
            {
                id = 999,
                permission = new List<int>() { 1, 2, 3, 4, 5, 6 }
            };

            Mirror.Assign(ref data, data2, data3);
            Trace.WriteLine(JsonConvert.SerializeObject(data));
            Assert.IsTrue(data.id == 999);
            Assert.IsTrue(data.name == "Zap");
        }

        [TestMethod()]
        public void AssignDictionaryTest()
        {
            Dictionary<string, int> data = new Dictionary<string, int>()
            {
                {"Zap",1 },
                {"Mary",2 },
                {"Lucy",3 },
            };

            var data2 = new
            {
                Zap = 99,
                Mary = "999999",
            };

            var data3 = new
            {
                Lucy = 999,
                Zap = true,
                Permission = new List<int>() { 1, 2, 3, 4, 5, 6 }
            };

            Mirror.Assign(ref data, data2, data3);

            Trace.WriteLine(JsonConvert.SerializeObject(data));
            Assert.IsTrue(data["Zap"] == 1);
            Assert.IsTrue(data["Mary"] == 999999);
            Assert.IsTrue(data["Lucy"] == 999);
        }

        [TestMethod()]
        public void EachMembersTest()
        {
            string[] keys = new string[] { "id", "name", "permission" };
            string[] vals = new string[] { "100", "zap", null };
            int i = 0;
            var data = new ModelTest() {
                id = 100,
                name = "zap",
                permission = new List<int>() { 1,2,3,4,5}
            };
            Mirror.EachMembers(data, (string key, string val) =>
            {
                Trace.WriteLine("key: " + key + " val: " + val);
                Assert.AreEqual(keys[i], key);
                Assert.AreEqual(vals[i], val);
                i++;
            });
        }
    }


    interface ITest { }
    class ModelTest : ITest
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<int> permission { get; set; }
    }
}