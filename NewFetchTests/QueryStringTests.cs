﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewFetch;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFetch.Tests
{
    [TestClass()]
    public class QueryStringTests
    {
        [TestMethod()]
        public void ObjectifyTest()
        {
            string qs = "name=zap&id=999&is_admin=True";
            ModelTest obj = QueryString.Objectify<ModelTest>(qs);
            Trace.WriteLine(JsonConvert.SerializeObject(obj));
            Assert.IsTrue(obj.name == "zap");
            Assert.IsTrue(obj.id == 999);
            Assert.IsTrue(obj.permission == null);
        }

        [TestMethod()]
        public void ObjectifyExtremeTest()
        {
            string qs = "wweicewicwiecweicnwec%26name%3Dnull%26id%3D%26is_admin%3DTrue";
            ModelTest obj = QueryString.Objectify<ModelTest>(qs);
            Trace.WriteLine(JsonConvert.SerializeObject(obj));
            Assert.IsTrue(obj.name == "null");
            Assert.IsTrue(obj.id == 0);
            Assert.IsTrue(obj.permission == null);
        }

        [TestMethod()]
        public void ParseObjectTest()
        {
            var data = new {
                id = 99,
                name  = "zap",
                is_admin = true,
                des = (string) null,
                permission = new List<int>() { 1,2,3,4,5}
            };

            string qs = QueryString.Parse(data);
            Trace.WriteLine(qs);
            Assert.AreEqual("id=99&name=zap&is_admin=True", qs);
        }

        [TestMethod()]
        public void ParseDirectoryTest()
        {
            var data = new Dictionary<string, string>() {
                { "id","99"},
                { "name","zap"},
                { "is_admin","True"},
            };

            string qs = QueryString.Parse(data);
            Trace.WriteLine(qs);
            Assert.AreEqual("id=99&name=zap&is_admin=True", qs);
        }
    }

}