﻿using System;
using System.Net;
using System.Web;

namespace NewFetch
{
    public static class QueryString
    {
        public static T Objectify<T>(string qs)
        {
            qs = WebUtility.UrlDecode(qs ?? string.Empty);
            var query = HttpUtility.ParseQueryString(qs);
            try { 
                T obj = (T)Activator.CreateInstance(typeof(T));
                foreach (string key in query)
                    Mirror.AssignValue(ref obj, key, query[key]); 
                return obj;
            }
            catch
            {
                return default;
            }
        }

        public static string Parse(object data)
        {
            var query = HttpUtility.ParseQueryString(string.Empty);
            Mirror.EachMembers(data, (string key, string val) =>
            {
                if (string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(val)) return;
                query[key] = val;
            });
            return query.ToString();
        }
    }
}


