﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFetch
{
    interface IQueryString
    {
        string Parse(object data);
        T Objectify<T>(string qs);
    }
}
