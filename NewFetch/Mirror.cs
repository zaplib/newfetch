﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace NewFetch
{
    public static class Mirror
    {
        public static IEnumerable<DictionaryEntry> Members(object obj)
        {
            if (obj == null) yield break;
            if (Cast.IsType<IDictionary>(obj))
                foreach (DictionaryEntry member in (IDictionary)obj)
                    yield return member;
            else
                foreach (var prop in obj.GetType().GetProperties())
                    yield return new DictionaryEntry(prop.Name, prop.GetValue(obj, null));
        }

        public static void EachMembers<TKey, TVal>(object obj, Action<TKey, TVal> cb)
        {
            if (cb == null) return;
            foreach (var kv in Members(obj))
                cb(Cast.To<TKey>(kv.Key), Cast.To<TVal>(kv.Value));
        }


        public static void Assign<T>(ref T target, params object[] objs)
        {
            foreach (object obj in objs)
                foreach (var kv in Members(obj))
                    AssignValue(ref target, kv.Key, kv.Value);
        }


        public static bool AssignValue<T>(ref T target, object key, object value)
        {
            try
            {
                if (Cast.IsType<IDictionary>(target))
                {
                    IDictionary dit = (IDictionary)target;
                    if (dit.Contains(key))
                    {
                        value = Cast.To(value, dit.GetType().GetGenericArguments()[1]);
                        if (value != null) dit[key] = value;
                    }
                }
                else
                {
                    var prop = target.GetType().GetProperty(Convert.ToString(key));
                    if (prop != null && prop.CanWrite)
                    {
                        value = Cast.To(value, prop.PropertyType);
                        if (value != null) prop.SetValue(target, value);
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString());
                return false;
            }
            return true;
        }

    }
}
