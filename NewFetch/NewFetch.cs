﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace NewFetch
{
    public class NewFetch
    {
        public HttpClient Client { get; set; }
        public HttpRequestMessage Request { get; set; }
        public HttpResponseMessage Response { private set; get; }


        public string Url
        {
            get => Client.BaseAddress?.ToString();
            set => Client.BaseAddress = new Uri(value ?? "http://localhost");
        }

        public object Qs
        {
            get => Client.BaseAddress.Query;
            set
            {
                Url = string.Format("{0}{1}{2}{3}{4}",
                    Client.BaseAddress.Scheme,
                    Uri.SchemeDelimiter,
                    Client.BaseAddress.Authority,
                    Client.BaseAddress.AbsolutePath,
                    value == null ? "" : "?" + QueryString.Parse(value));
            }
        }

        public object Header
        {
            set
            {
                Mirror.EachMembers(value, (string key, string val) =>
                {
                    if (string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(val)) return;
                    switch (key.ToLower())
                    {
                        case "content-type": ContentType = val; break;
                        default: Client.DefaultRequestHeaders.Add(key, val); break;
                    }
                });
            }
            get => Client.DefaultRequestHeaders;
        }

        public object Cookie
        {
            set
            {
                Mirror.EachMembers(value, (string key, string val) =>
                {
                    if (string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(val)) return;
                    CookieContainer.Add(new Cookie(key, val) { Domain = Client.BaseAddress.Host });
                });
            }
            get => Client.BaseAddress == null ? null : CookieContainer.GetCookies(Client.BaseAddress);
        }

        public string Accept
        {
            get => Client.DefaultRequestHeaders.Accept.ToString();
            set => Client.DefaultRequestHeaders.Accept.TryParseAdd(value);
        }

        public string ContentType { get; set; }

        public bool ValidPlatform { get; set; }

        public int StatusCode
        {
            get => Response?.StatusCode == null ? 0 : (int)Response.StatusCode;
        }


        public Encoding RequestEncoding { get; set; } = Encoding.UTF8;


        // set the maxmium binaryResponse size (byte) default 25M
        public int MaxDownloadSize { get; set; } = 25 * 1024;

        private CookieContainer CookieContainer;

        public NewFetch(string uri = null)
        {
            CookieContainer = new CookieContainer();
            Client = new HttpClient(new HttpClientHandler() { CookieContainer = CookieContainer });
            Request = new HttpRequestMessage();
            Url = uri;
        }

        ~NewFetch()
        {
            if (Response != null) Response.Dispose();
            if (Request != null) Request.Dispose();
            if (Client != null) Client.Dispose();
        }


        public string Get(object qs = null)
        {
            Qs = qs;
            Request.Method = HttpMethod.Get;
            return Send() ? Response.Content.ReadAsStringAsync().Result : null;
        }

        public T Get<T>(object qs = null)
        {
            Qs = qs;
            Request.Method = HttpMethod.Get;
            Accept = "application/json";
            return Send() ? JsonConvert.DeserializeObject<T>(Response.Content.ReadAsStringAsync().Result) : default;
        }

        public byte[] GetBinary(object qs = null)
        {
            Qs = qs;
            Request.Method = HttpMethod.Get;
            if (Send())
                try
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (var stream = Response.Content.ReadAsStreamAsync().Result)
                        {
                            byte[] buffer = new byte[MaxDownloadSize];
                            int read;
                            while ((read = stream.Read(buffer, 0, buffer.Length)) > 0) ms.Write(buffer, 0, read);
                            return ms.ToArray();
                        }
                    }
                }
                catch { }
            return null;
        }

        public string Post(object data = null, object qs = null, object files = null)
        {
            Qs = qs;
            if (files != null) ContentType = "multipart/form-data";
            Request.Method = HttpMethod.Post;
            SetRequestContnet(data, files);
            return Send() ? Response.Content.ReadAsStringAsync().Result : default;
        }

        public T Post<T>(object data = null, object qs = null, object files = null)
        {
            Qs = qs;
            Accept = "application/json";
            Request.Method = HttpMethod.Post;
            SetRequestContnet(data, files);
            return Send() ? JsonConvert.DeserializeObject<T>(Response.Content.ReadAsStringAsync().Result) : default;
        }

        public string Delete(object data = null, object qs = null, object files = null)
        {
            Qs = qs;
            Request.Method = HttpMethod.Delete;
            SetRequestContnet(data, files);
            return Send() ? Response.Content.ReadAsStringAsync().Result : default;
        }

        public T Delete<T>(object data = null, object qs = null, object files = null)
        {
            Qs = qs;
            Accept = "application/json";
            Request.Method = HttpMethod.Delete;
            SetRequestContnet(data, files);
            return Send() ? JsonConvert.DeserializeObject<T>(Response.Content.ReadAsStringAsync().Result) : default;
        }


        public string Put(object data = null, object qs = null, object files = null)
        {
            Qs = qs;
            Request.Method = HttpMethod.Put;
            SetRequestContnet(data, files);
            return Send() ? Response.Content.ReadAsStringAsync().Result : null;
        }

        public T Put<T>(object data = null, object qs = null, object files = null)
        {
            Qs = qs;
            Accept = "application/json";
            Request.Method = HttpMethod.Put;
            SetRequestContnet(data, files);
            return Send() ? JsonConvert.DeserializeObject<T>(Response.Content.ReadAsStringAsync().Result) : default;
        }

        public string GetResponse() => Response.Content?.ReadAsStringAsync().Result;

        public byte[] GetBinaryResponse() => Response.Content?.ReadAsByteArrayAsync().Result;

        public HttpResponseHeaders GetResponseHeaders() => Response?.Headers;

        public string GetResponseHeader(string key)
        {
            if (Response != null)
                if (Response.Headers.TryGetValues(key, out IEnumerable<string> val))
                    return val.First();
            return null;
        }

        public MultipartFormDataContent BuildMultipartFormDataContent(object data = null, object files = null)
        {
            if (data == null && files == null) return null;
            var form = new MultipartFormDataContent();
            if (data != null)
                Mirror.EachMembers(data, (string key, string val) =>
                {
                    if (string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(val)) return;
                    form.Add(new StringContent(val), key);
                });
            if (files != null)
                Mirror.EachMembers(data, (string key, string val) =>
                {
                    if (string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(val)) return;
                    val = val.Trim();
                    if (!File.Exists(val)) return;
                    form.Add(new StreamContent(File.OpenRead(val)), key, Path.GetFileName(val));
                });
            return form;
        }

        public FormUrlEncodedContent BuildFormUrlEncodedContent(object data)
        {
            if (data == null) return null;
            List<KeyValuePair<string, string>> kvCollection = new List<KeyValuePair<string, string>>();
            Mirror.EachMembers(data, (string key, string val) =>
            {
                if (string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(val)) return;
                kvCollection.Add(new KeyValuePair<string, string>(key, val));
            });
            return new FormUrlEncodedContent(kvCollection);
        }

        public StringContent BuildStringContent(string data = null) =>
            data == null ?
            null :
            new StringContent(data, RequestEncoding, ContentType);

        private void SetRequestContnet(object data = null, object files = null)
        {
            if (string.IsNullOrWhiteSpace(ContentType)) ContentType = "application/json";
            if (files != null) ContentType = "multipart/form-data";

            switch (ContentType)
            {
                case "application/x-www-form-urlencoded":
                    Request.Content = BuildFormUrlEncodedContent(data);
                    break;
                case "multipart/form-data":
                    Request.Content = BuildMultipartFormDataContent(data, files);
                    break;
                case "text/json":
                case "application/json":
                    Request.Content = BuildStringContent(JsonConvert.SerializeObject(data));
                    break;
                default:
                    Request.Content = BuildStringContent(Cast.To<string>(data));
                    break;
            }

        }

        public bool Send()
        {
            if (StatusCode != 0)
                throw new Exception("Every Fetch instance only send request once!");
            Response = Client.SendAsync(Request).Result;
            return Response.IsSuccessStatusCode;
        }
    }
}
