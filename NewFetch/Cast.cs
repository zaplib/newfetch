﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFetch
{
    public static class Cast
    {
        public static bool IsType<T>(object obj)
        {
            return obj == null ? CanBeNull(typeof(T)) :
                (obj is T || typeof(T).IsAssignableFrom(obj.GetType()));
        }

        public static bool IsType(object obj1, object obj2)
        {
            if (obj1 == null && obj2 == null) return true;
            else if (obj1 != null && obj2 == null) return CanBeNull(obj1.GetType());
            else if (obj1 == null && obj2 != null) return CanBeNull(obj2.GetType());
            else
            {
                Type t1 = obj1.GetType();
                Type t2 = obj2.GetType();
                return (t1.Equals(t2) || t2.IsAssignableFrom(t1));
            }
        }

        public static bool CanBeNull(Type t)
        {
            return !t.IsValueType || (Nullable.GetUnderlyingType(t) != null);
        }


        public static T To<T>(object obj, T def_val = default)
        {
            try
            {
                return (T)Convert.ChangeType(obj, typeof(T));
            }
            catch
            {
                return def_val;
            }
        }

        public static object To(object obj, Type targetType)
        {
            try
            {
                return Convert.ChangeType(obj, targetType);
            }
            catch
            {
                return targetType.IsValueType ? Activator.CreateInstance(targetType) : null;
            }
        }
    }
}
