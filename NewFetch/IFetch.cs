﻿/*
    ZapLib 規格定義介面 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NewFetch
{
    /// <summary>
    /// ZapLib 規格定義
    /// </summary>
    interface IFetch
    {
        string get(object qs = null);
        T get<T>(object qs = null);
        byte[] getBinary(object qs = null);

        string post(object data = null, object qs = null, object files = null);
        T post<T>(object data = null, object qs = null, object files = null);

        string delete(object data = null, object qs = null);
        T delete<T>(object data = null, object qs=null);

        string getResponse();
        byte[] getBinaryResponse();
        int getStatusCode();

        WebHeaderCollection getResponseHeaders();
        string getResponseHeader(string key);
    }
}
